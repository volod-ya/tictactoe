import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {GameDifficultService} from '../services/game-difficult.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-play-with-computer',
  templateUrl: './play-with-computer.component.html',
  styleUrls: ['./play-with-computer.component.css']
})
export class PlayWithComputerComponent implements OnInit {
  @Output() goBack: EventEmitter<any> = new EventEmitter<any>();
  public currentDifficultySubscription: Subscription;
  public currentDifficulty: boolean;

  constructor(private gameDifficultService: GameDifficultService) {
  }

  ngOnInit() {
    this.currentDifficulty = false;
    this.currentDifficultySubscription = this.gameDifficultService.getGameDifficult()
      .subscribe((difficulty: boolean) => {
        this.currentDifficulty = difficulty;
      });
  }

  returnBack() {
    this.goBack.emit();
  }

  setGameDifficult(difficulty) {
    this.gameDifficultService.setgameDifficult(difficulty);
  }

}
