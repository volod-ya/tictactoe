import {Injectable} from '@angular/core';
import {User} from '../models/user.model';
import {Subject} from 'rxjs/Subject';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserService {
  userSubject: Subject<User> = new Subject<User>();
  // private user: User = new User( 'Volod', 'Qwerty@qwe', '123qwe');
  private user: User;

  constructor(private http: HttpClient) {
  }

  getUser(): User {
    return this.user;
  }

  subscribeOnUser(): Observable<User> {
    return this.userSubject.asObservable();
  }

  setUser(user: User) {
    this.user = user;
    this.userSubject.next(user);
  }

  loadUser() {
    // this.setUser();
  }

}
