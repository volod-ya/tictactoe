import {Injectable} from '@angular/core';
import {GameMode} from '../enums/game-mode.enum';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GameModeService {

    private gameModeSubject = new Subject<GameMode>();

    constructor() {
    }

    getGameMode(): Observable<any> {
        return this.gameModeSubject.asObservable();
    }

    setGameMode(mode: GameMode) {
        this.gameModeSubject.next(GameMode[mode]);
    }

}
