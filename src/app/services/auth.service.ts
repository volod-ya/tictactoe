import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {catchError} from 'rxjs/operators';
import {User} from '../models/user.model';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
    })
};

// 'Authorization': 'my-auth-token'

@Injectable()
export class AuthService {

    constructor(private httpClient: HttpClient) {
    }

    signIn(user: any) {
        const body = JSON.stringify(user);
        console.log(environment.url + 'auth/signin/');
        return this.httpClient
            .post(environment.url + 'auth/signin', body, httpOptions)
            .pipe(
                catchError((error: Response) => {
                    return Observable.throw(error.json());
                })
            );
    }

    signUp(user: User) {
        const body = JSON.stringify(user);
        console.log(environment.url + 'auth/signup', body, httpOptions);
        return this.httpClient
            .post(environment.url + 'auth/signup', body, httpOptions)
            .pipe(
                catchError((error: Response) => {
                    return Observable.throw(error.json());
                })
            );
    }

}




