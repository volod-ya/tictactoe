import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {Chat} from '../models/chat.model';
import {Message} from "../models/message.model";

@Injectable()
export class ChatListService {
  private _chatList: Chat[] = [];
  private chatListSubject: Subject<Chat[]> = new Subject<Chat[]>();

  private _currentChat: Chat;

  // private currentChatSubject: Chat;

  constructor() {
  }

  get chatList() {
    return this._chatList.splice;
  }

  subscribeOnChatList(): Observable<Chat[]> {
    return this.chatListSubject.asObservable();
  }

  openChat(index: number) {
    this._currentChat = this._chatList[index];
    return this._currentChat;
  }

  createChat(companionId: string, time: number, messageContent: any): void {
    // send request to backend for createting chat with
    // userList: companionId, currentUserId for creating chat
    // for creating message:  createdAt: time,  senderId: currentUserId,
    // content: messageContent, readUserId: currentUserId ;
  }

  removeChat(index: number): void {
    this._chatList.splice(index, 1);
    this.chatListSubject.next(this._chatList);
  }

  openRoom() {
    // when user want to talk with somebody from human list
    // he click on it and open room and while he doesn't send a message it only room
    // which will be closed when user go to another room or char
  }


}
