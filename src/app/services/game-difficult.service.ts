import { Injectable } from '@angular/core';
import {GameMode} from '../enums/game-mode.enum';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class GameDifficultService {

  private gameDifficultSubject = new Subject<boolean>();

  constructor() {
  }

  getGameDifficult(): Observable<boolean> {
    return this.gameDifficultSubject.asObservable();
  }

  setgameDifficult(difficulty: boolean): void {
    this.gameDifficultSubject.next(difficulty);
  }

}
