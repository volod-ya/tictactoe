import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router,
    ) {}

    canActivate(route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return new Promise((resolve, reject) => {
            // uf user exist and localStorage.getItem('jwtToken')
            // if (this.userService.getUser() && localStorage.getItem('jwtToken')) {
            //     resolve(true);
            // } else {
            //     this.router.navigate(['login']);
            //     resolve(false);
            // }
            resolve(true);
        });
    }
}
