import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../models/user.model';
import {Subscription} from 'rxjs/Subscription';
import {UserService} from '../services/user.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  signInStatus: { isopen: boolean } = {isopen: false};
  signUpStatus: { isopen: boolean } = {isopen: false};

  userSubscription: Subscription;
  user: User;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.user = this.userService.getUser();
    this.userSubscription = this.userService.subscribeOnUser()
      .subscribe((user: User) => this.user = user);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }


  toggleSignIn($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.signInStatus.isopen = !this.signInStatus.isopen;
  }

  changeSignIn(value: boolean): void {
    this.signInStatus.isopen = value;
    if (value) {
      this.signUpStatus.isopen = false;
    }
  }

  toggleSignUp($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.signUpStatus.isopen = !this.signUpStatus.isopen;
  }

  changeSignUp(value: boolean): void {
    this.signUpStatus.isopen = value;
    if (value) {
      this.signInStatus.isopen = false;
    }
  }

  goToMain() {
    this.router.navigate(['/']);
  }

}
