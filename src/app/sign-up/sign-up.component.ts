import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ControlContainer, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from "../services/auth.service";

@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
    @Output() closeDropdown: EventEmitter<any> = new EventEmitter<any>();
    public signUpForm: FormGroup;
    public avatar: any;
    public maxImageSize = 5242880;


    constructor(private authService: AuthService) {
    }

    ngOnInit() {
        this.signUpForm = new FormGroup({
            'login': new FormControl(null, Validators.required),
            'email': new FormControl(null, [Validators.required, Validators.email]),
            'password': new FormControl(null, [Validators.required]),
            'conf-password': new FormControl(null, Validators.required),
            'avatar': new FormControl(null),
        });
    }

    previewImage($event) {
        this.avatar = $event.target.files[0];
    }

    countRudeImageSize(size: number) {
        return Math.ceil((size * 100) / 1048576) / 100;
    }

    resetFile() {
        this.avatar = null;
    }

    onSubmit() {
        if (this.signUpForm.invalid) {
            this.signUpForm.markAsTouched();
            for (const control in this.signUpForm.controls) {
                if (this.signUpForm.controls.hasOwnProperty(control)) {
                    this.signUpForm.controls[control].markAsTouched();
                }
            }
        } else {
            const sendingData = {
                login: this.signUpForm.get('login').value,
                email: this.signUpForm.get('email').value,
                password: this.signUpForm.get('password').value,
                avatar: this.avatar && this.avatar.size <= this.maxImageSize ? this.avatar : null
            };
            this.authService.signUp(sendingData)
                .subscribe((resp) => {
                    console.log(sendingData);
                    this.signUpForm.reset();
                    this.avatar = null;
                    this.closeDropdown.emit();
                });
        }
    }
}
