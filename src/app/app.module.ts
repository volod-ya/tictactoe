import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './/app-routing.module';
import {HeaderComponent} from './header/header.component';
import {GameFieldComponent} from './game-field/game-field.component';
import {GameOptionsComponent} from './game-options/game-options.component';
import {FooterComponent} from './footer/footer.component';
import {MessagesComponent} from './messages/messages.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {GameModeService} from './services/game-mode.service';
import {PlayWithComputerComponent} from './play-with-computer/play-with-computer.component';
import {GameDifficultService} from './services/game-difficult.service';
import {UserListComponent} from './user-list/user-list.component';
import {ChatListComponent} from './chat-list/chat-list.component';
import {SignInComponent} from './sign-in/sign-in.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {ControlContainer, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {UserService} from './services/user.service';
import { SettingsComponent } from './settings/settings.component';
import { ChatComponent } from './chat-list/chat/chat.component';
import { ContactMeComponent } from './contact-me/contact-me.component';
import { AboutMeComponent } from './about-me/about-me.component';
import { ContactsComponent } from './contacts/contacts.component';
import {AuthService} from "./services/auth.service";


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GameFieldComponent,
    GameOptionsComponent,
    FooterComponent,
    MessagesComponent,
    PlayWithComputerComponent,
    UserListComponent,
    ChatListComponent,
    SignInComponent,
    SignUpComponent,
    SettingsComponent,
    ChatComponent,
    ContactMeComponent,
    AboutMeComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [GameModeService, GameDifficultService, UserService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
