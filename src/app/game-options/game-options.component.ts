import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameModeService} from '../services/game-mode.service';
import {GameMode} from '../enums/game-mode.enum';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-game-options',
  templateUrl: './game-options.component.html',
  styleUrls: ['./game-options.component.css']
})
export class GameOptionsComponent implements OnInit, OnDestroy {
  public gameMode: GameMode = GameMode.notChose;
  gameModeSubscription: Subscription;

  constructor(private gameModeService: GameModeService) {
  }

  ngOnInit() {
    this.gameModeSubscription = this.gameModeService.getGameMode()
      .subscribe((mode: GameMode) => {
        this.gameMode = mode;
      });
  }

  ngOnDestroy() {
    this.gameModeSubscription.unsubscribe();
  }

  setGameMode(mode: GameMode): void {

    this.gameModeService.setGameMode(mode);
  }
}
