import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {ChatListComponent} from './chat-list/chat-list.component';
import {SettingsComponent} from './settings/settings.component';
import {ChatComponent} from './chat-list/chat/chat.component';
import {ContactMeComponent} from "./contact-me/contact-me.component";
import {AboutMeComponent} from "./about-me/about-me.component";
import {ContactsComponent} from "./contacts/contacts.component";

const routes: Routes = [
  {path: '', component: ChatListComponent, pathMatch: 'full'},
  {path: 'index', redirectTo: ''},
  {path: 'chat-list', redirectTo: ''},
  {path: 'chat-list/:chat_id', component: ChatComponent},
  {path: 'settings', component: SettingsComponent},
  {path: 'contact-me', component: ContactMeComponent},
  {path: 'about-me', component: AboutMeComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', redirectTo: ''},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
