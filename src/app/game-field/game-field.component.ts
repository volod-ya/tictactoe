import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {GameDifficultService} from '../services/game-difficult.service';
import {GameModeService} from '../services/game-mode.service';
import {GameMode} from '../enums/game-mode.enum';
import {Player} from '../models/player.model';


@Component({
  selector: 'app-game-field',
  templateUrl: './game-field.component.html',
  styleUrls: ['./game-field.component.css']
})
export class GameFieldComponent implements OnInit, OnDestroy {

  public gameStartedStatus = false;
  public squares: any[] = Array.from(Array(9).keys());
  public message: string;
  player: Player;
  opponent: Player;
  winner = null;
  playWithAI: boolean;
  opponentMakesMoveStatus: boolean;
  public difficultRound: boolean;
  gameDifficultSubscription: Subscription;
  gameModeSubscription: Subscription;

  winConditions: any = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [6, 4, 2]
  ];

  constructor(private gameDifficultService: GameDifficultService,
              private gameModeService: GameModeService) {
  }

  static getRandom(max) {
    return Math.round(Math.random() * max);
  }

  isNumber(val) {
    return typeof val === 'number';
  }

  ngOnInit() {
    this.difficultRound = false;
    this.playWithAI = true;

    this.gameDifficultSubscription = this.gameDifficultService.getGameDifficult()
      .subscribe((difficulty: boolean) => {
        this.difficultRound = difficulty;
      });
    this.gameModeSubscription = this.gameModeService.getGameMode()
      .subscribe((mode: GameMode) => {
        this.playWithAI = mode !== 'human';
      });

    this.opponentMakesMoveStatus = false;
  }

  ngOnDestroy() {
    this.gameDifficultSubscription.unsubscribe();
    this.gameModeSubscription.unsubscribe();
  }

  get gameMessage() {
    return this.message;
  }

  set gameMessage(message: string) {
    this.message = message;
  }

  get emptyCells() {
    return this.squares.filter(c => typeof c === 'number');
  }

  selectPlayer(symbol) {
    this.player = new Player(symbol, 'human');
    if (this.playWithAI) {
      const opponentSymbol: string = (symbol === 'X') ? 'O' : 'X';
      this.opponent = new Player(opponentSymbol, 'ai');
      if (opponentSymbol === 'X') {
        this.aiMove();
      }
    }

    this.gameStartedStatus = true;
    this.gameMessage = 'Make a move';
  }

  processMove(position) {
    if (!this.player) {
      this.gameMessage = 'Please select your symbol';
      return;
    }
    if (typeof this.squares[position] === 'number') {
      this.squares[position] = this.player.symbol;
      if (this.checkWin(this.squares, this.player.symbol)) {
        this.winner = this.player.symbol;
        this.gameMessage = `Player ${this.winner} has won!`;
        return;
      }
      if (!this.checkTieDraw()) {
        this.opponentMakesMoveStatus = true;
      }
      if (this.playWithAI) {
        this.aiMove();
      } else {
        this.gameMessage = `Player ${this.opponent.symbol}"s turn`;
      }
    } else {
      this.gameMessage = 'This field is already occupied';
    }
  }

  aiMove() {
    this.gameMessage = 'Computting...';
    setTimeout(() => {
      if (this.difficultRound) {
        this.squares[this.minimax(this.squares, this.opponent.symbol).index] = this.opponent.symbol;
      } else {
        const freeCells = this.emptyCells;
        this.squares[freeCells[GameFieldComponent.getRandom(freeCells.length - 1)]] = this.opponent.symbol;
      }
      this.opponentMakesMoveStatus = false;
      this.gameMessage = `Player ${this.player.symbol}"s turn`;
    }, 300);
  }

  checkWin(board, player) {
    const place = board.reduce(
      (arr, elem, index) => (elem === player) ? arr.concat(index) : arr, []
    );
    let gameWon = null;
    for (const item of this.winConditions) {
      if (item.every(elem => place.indexOf(elem) > -1)) {
        gameWon = player;
        break;
      }
    }
    return gameWon;
  }

  startNewGame() {
    this.squares = Array.from(Array(9).keys());
    this.player = null;
    this.opponent = null;
    this.winner = null;
    this.gameMessage = '';
    this.gameStartedStatus = false;
  }

  checkTieDraw() {
    if (this.emptyCells.length === 0) {
      this.gameMessage = 'This was a draw';
      return true;
    }
    return false;
  }

  minimax(gameBoard, player): any {
    const availCells = this.emptyCells;
    const moves = [];
    if (this.checkWin(gameBoard, this.player.symbol)) {
      return {score: -10};
    } else if (this.checkWin(gameBoard, this.opponent.symbol)) {
      return {score: 10};
    } else if (availCells.length === 0) {
      return {score: 0};
    }

    for (let i = 0; i < availCells.length; i++) {
      const move: any = {};
      move.index = gameBoard[availCells[i]];

      gameBoard[availCells[i]] = player;

      if (player === this.opponent.symbol) {
        const result = this.minimax(gameBoard, this.player.symbol);
        move.score = result.score;
      } else {
        const result = this.minimax(gameBoard, this.opponent.symbol);
        move.score = result.score;
      }

      gameBoard[availCells[i]] = move.index;

      moves.push(move);
    }

    let bestMove;
    if (player === this.opponent.symbol) {
      let bestScore = -10000;
      for (let i = 0; i < moves.length; i++) {
        if (moves[i].score > bestScore) {
          bestScore = moves[i].score;
          bestMove = i;
        }
      }
    } else {

      let bestScore = 10000;
      for (let i = 0; i < moves.length; i++) {
        if (moves[i].score < bestScore) {
          bestScore = moves[i].score;
          bestMove = i;
        }
      }
    }

    return moves[bestMove];
  }


}
