import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  @Output() closeDropdown: EventEmitter<any> = new EventEmitter<any>();
  public signInForm: FormGroup;

  sendingData: any;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.signInForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required]),
    });
  }
  onSubmit() {
    if (this.signInForm.invalid) {
      this.signInForm.markAsTouched();
      for (const control in this.signInForm.controls) {
        if (this.signInForm.controls.hasOwnProperty(control)) {
          this.signInForm.controls[control].markAsTouched();
        }
      }
    } else {
      this.sendingData = {
        email: this.signInForm.get('email').value,
        password: this.signInForm.get('password').value,
      };
      console.log(this.sendingData);
      this.authService.signIn(this.sendingData)
          .subscribe((data) => {
            console.log('data: ', data);
          }, (error) => {
            console.log('error: ', error);
          });
      this.signInForm.reset();
      this.closeDropdown.emit();
    }
  }
}
