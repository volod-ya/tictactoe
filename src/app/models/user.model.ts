export class User {
  login: string;
  email: string;
  password: string;
  avatar: any;

  constructor(login: string, email: string, password: string, avatar?: any) {
    this.login = login;
    this.email = email;
    this.password = password;
    this.avatar = avatar;
  }
}
