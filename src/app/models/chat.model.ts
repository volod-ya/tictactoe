export class Chat {
  chatId: string;
  chatTitle: string;
  userList: string[];
  chatLastMessageId: string;

  constructor(chatId: string, chatTitle: string, userList: string[], chatLastMessageId?: string) {
    this.chatId = chatId;
    this.chatTitle = chatTitle;
    this.userList = userList;
    this.chatLastMessageId = chatLastMessageId;
  }
}
