export class Player {
  symbol: string;
  type: string;

  constructor(symbol?: string, type?: string) {
    this.symbol = symbol;
    this.type = type;
  }
}
