export class Message {
  messageId: string;
  chatId: string;
  createdAt: number;
  senderId: string;
  content: any;
  readUserId: string[];

  constructor(messageId: string, chatId: string, createdAt: number, senderId: string, content: string, readUserId: string[]) {
    this.messageId = messageId;
    this.chatId = chatId;
    this.createdAt = createdAt;
    this.senderId = senderId;
    this.content = content;
    this.readUserId = readUserId;
  }
}

